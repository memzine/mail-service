var api_key = 'key-d5c6748634b7fcd01e5024ceb2e7038a';
var domain = 'mail.memzine.co';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
var { hasKeys } = require('./tools')
/**
 * Uses mailgun to send mail.
 * @param data = { from: String, to: String, html: String, subject: String }
 * 
 * Checks if keys exist if not throws
 * 
 * Uses mailgun api to send 
 */

const sendMail = async (data) => {
  return new Promise((resolve, reject) => {
    console.log(data)
    if (!hasKeys(['from', 'to', 'html', 'subject'], data)) {
      reject({
        code: 400,
        message: 'Data Object must include the following keys: from, to, html, subject'
      })
      return
    }
    mailgun.messages().send(data, function (error, body) {
      if (error) {
        reject(error)
      }
      resolve(body)
    });
  })
}

module.exports = sendMail