function hasKeys (requiredKeys, obj) {
  return requiredKeys.every(k => k in obj)
 }

 module.exports = {
   hasKeys
 }