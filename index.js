const Koa = require('koa')
const koaBody = require('koa-body')
const app = new Koa()
const sendMail = require('./sendMail')
const fs = require('fs')
const util = require('util')

const writeFile = util.promisify(fs.writeFile)

app.use(koaBody())

/**
 * Mail server
 * Simply sends mail of any kind.
 * If successful, response with Mail sent to
 */

app.use(async (ctx) => {
  try {
    const body = ctx.request.body
    const data = body.emailData
    const mailResponse = await sendMail(data)
    ctx.status = 200
    ctx.body = {
      success: true,
      message: `Mail was sent to ${data.to}`,
      mailgunResponse: mailResponse
    }
  } catch (error) {
    console.log(error)
    const code = error.code || 500
    ctx.status = code
    ctx.body = {
      code,
      message: error.message
    }
  }
})

app.listen(process.env.PORT || 3000)